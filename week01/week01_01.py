import requests
from datetime import date, datetime
import json

ACCESS_TOKEN = 'INSERT YOUR ACCESS TOKEN'


def calc_age(uid):
    age_dict = {}

    resp = requests.get(
        'https://api.vk.com/method/friends.get?v=5.71&access_token=' + ACCESS_TOKEN + '&user_id=' + uid + '&fields=bdate'
        )
    response = json.loads(resp.text)["response"]["items"]

    for friend in response:
        try:
            birth_date = friend['bdate'].split('.')
            try:
                age = datetime.now().year - int(birth_date[2])
                if age not in age_dict:
                    age_dict[age] = 0
                age_dict[age] += 1
            except IndexError:
                pass
        except KeyError:
            pass
    sorted_list = sorted(age_dict.items(), key=lambda x: (x[1], -x[0]), reverse=True)
    return sorted_list


if __name__ == '__main__':
    res = calc_age('21205172')
    print(res)
