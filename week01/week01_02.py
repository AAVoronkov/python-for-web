def calculate(data, findall):
    matches = findall(r"([abc])([+-]?)=([bc]?)([+-]?\d+)?")
    for v1, s, v2, n in matches:
        if s == '+':
            if not v2:
                v2 = v1
                data[v1] += int(n or 0)
            else:
                data[v1] += data[v2] + int(n or 0)
        elif s == '-':
            if not v2:
                v2 = v1
                data[v1] -= int(n or 0)
            else:
                data[v1] -= data[v2] + int(n or 0)
        else:
            data[v1] = data.get(v2, 0) + int(n or 0)

    return data
