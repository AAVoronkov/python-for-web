use test
set names utf8;

-- 1. Выбрать все товары (все поля)
SELECT * FROM product

-- 2. Выбрать названия всех автоматизированных складов
SELECT name FROM store WHERE is_automated=1;

-- 3. Посчитать общую сумму в деньгах всех продаж
SELECT sum(total) FROM sale;

-- 4. Получить уникальные store_id всех складов, с которых была хоть одна продажа
SELECT store_id FROM sale WHERE quantity IS NOT NULL GROUP BY store_id;

-- 5. Получить уникальные store_id всех складов, с которых не было ни одной продажи
SELECT store_id FROM store NATURAL LEFT JOIN sale WHERE quantity IS NULL;

-- 6. Получить для каждого товара название и среднюю стоимость единицы товара avg(total/quantity), если товар не продавался, он не попадает в отчет.
SELECT name, avg(total/quantity) FROM product NATURAL LEFT JOIN sale WHERE quantity IS NOT NULL GROUP BY name;

-- 7. Получить названия всех продуктов, которые продавались только с единственного склада
SELECT name FROM sale NATURAL LEFT JOIN product GROUP BY name HAVING count(distinct(store_id))=1;

-- 8. Получить названия всех складов, с которых продавался только один продукт
SELECT name FROM store NATURAL LEFT JOIN sale GROUP BY name HAVING count(distinct(product_id))=1;

-- 9. Выберите все ряды (все поля) из продаж, в которых сумма продажи (total) максимальна (равна максимальной из всех встречающихся)
SELECT * FROM sale WHERE total = (SELECT MAX(total) FROM sale);

-- 10. Выведите дату самых максимальных продаж, если таких дат несколько, то самую раннюю из них
SELECT date FROM sale WHERE total = (SELECT MAX(total) FROM sale) GROUP BY date LIMIT 1;
