from __future__ import absolute_import, unicode_literals
from celery import task
from django.conf import settings
from django.core.mail import EmailMessage
from .models import Setting
import requests


TOKEN = settings.SMART_HOME_ACCESS_TOKEN
URL = settings.SMART_HOME_API_URL


@task()
def smart_home_manager():
    controller_data = requests.get(
        URL,
        headers={'Authorization': f'Bearer {settings.SMART_HOME_ACCESS_TOKEN}'}
    ).json().get('data')
    controller_data = {x['name']: x for x in controller_data}
    json_data = {'controllers': []}

    # Протечка воды
    if controller_data['leak_detector']['value']:
        if controller_data['cold_water']['value']:
            json_data['controllers'].append({'name': 'cold_water', 'value': False})
        if controller_data['hot_water']['value']:
            json_data['controllers'].append({'name': 'hot_water', 'value': False})
        email = EmailMessage(
            'leak detector',
            'text',
            settings.EMAIL_HOST,
            [settings.EMAIL_RECEPIENT],
        )
        email.send(fail_silently=False)

    # Холодная вода закрыта
    if controller_data['leak_detector']['value'] or not controller_data['cold_water']['value']:
        if controller_data['boiler']['value']:
            json_data['controllers'].append({'name': 'boiler', 'value': False})
        if controller_data['washing_machine']['value'] in ('on', 'broken'):
            json_data['controllers'].append({'name': 'washing_machine', 'value': "off"})

    # Включатель/выключатель
    switch = {
        'boiler': controller_data['cold_water']['value'] and
                  not controller_data['leak_detector']['value'] and
                  not controller_data['smoke_detector']['value'] and
                  not controller_data['boiler'],
        'air_conditioner': not controller_data['smoke_detector']['value']
    }

    # Бойлер
    boiler_temperature = controller_data['boiler_temperature']['value']
    hot_water_target_temperature = Setting.objects.get(
        controller_name='hot_water_target_temperature'
    ).value
    if (boiler_temperature < hot_water_target_temperature * 0.9) and switch['boiler']:
        json_data['controllers'].append({'name': 'boiler', 'value': True})
    if boiler_temperature > hot_water_target_temperature * 1.1:
        json_data['controllers'].append({'name': 'boiler', 'value': False})

    # Спальня
    bedroom_temperature = controller_data['bedroom_temperature']['value']
    bedroom_target_temperature = Setting.objects.get(
        controller_name='bedroom_target_temperature'
    ).value
    if (bedroom_temperature < bedroom_target_temperature * 0.9) and switch['air_conditioner']:
        json_data['controllers'].append({'name': 'air_conditioner', 'value': False})
    if bedroom_temperature > bedroom_target_temperature * 1.1:
        json_data['controllers'].append({'name': 'air_conditioner', 'value': True})

    # Освещённость
    outdoor_light = controller_data['outdoor_light']['value']
    bedroom_light = controller_data['bedroom_light']['value']
    if controller_data['curtains']['value'] == 'slightly_open':
        pass
    else:
        if outdoor_light < 50 and not bedroom_light:
            json_data['controllers'].append({'name': 'curtains', 'value': 'open'})
        elif outdoor_light > 50 or bedroom_light:
            json_data['controllers'].append({'name': 'curtains', 'value': 'close'})

    # Задымление
    if controller_data['smoke_detector']['value']:
        if controller_data['air_conditioner']['value']:
            json_data['controllers'].append({'name': 'air_conditioner', 'value': False})
        if controller_data['bathroom_light']['value']:
            json_data['controllers'].append({'name': 'bathroom_light', 'value': False})
        if controller_data['bedroom_light']['value']:
            json_data['controllers'].append({'name': 'bedroom_light', 'value': False})
        if controller_data['boiler']['value']:
            json_data['controllers'].append({'name': 'boiler', 'value': False})
        if controller_data['washing_machine']['value'] in ('on', 'broken'):
            json_data['controllers'].append({'name': 'washing_machine', 'value': 'off'})

    # Отправка данных
    if json_data['controllers']:
        controllers = []
        for unit in json_data['controllers']:
            if unit not in controllers:
                controllers.append(unit)
        json_data['controllers'] = controllers
        requests.post(
            URL,
            headers={'Authorization': f'Bearer {settings.SMART_HOME_ACCESS_TOKEN}'},
            json=json_data
        )
