from django import forms
from django.core.validators import MinValueValidator, MaxValueValidator


class ControllerForm(forms.Form):
    bedroom_target_temperature = forms.IntegerField(
        validators=[MinValueValidator(16), MaxValueValidator(50)],
        required=False
    )
    hot_water_target_temperature = forms.IntegerField(
        validators=[MinValueValidator(16), MaxValueValidator(50)],
        required=False
    )
    bedroom_light = forms.BooleanField(required=False)
    bathroom_light = forms.BooleanField(required=False)
