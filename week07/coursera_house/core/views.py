from django.urls import reverse_lazy
from django.views.generic import FormView
from django.conf import settings
from .models import Setting
from .form import ControllerForm

import requests


TOKEN = settings.SMART_HOME_ACCESS_TOKEN
URL = settings.SMART_HOME_API_URL


class ControllerView(FormView):
    form_class = ControllerForm
    template_name = 'core/control.html'
    success_url = reverse_lazy('form')

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        if not context.get('data'):
            return self.render_to_response(context, status=502)
        else:
            return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(ControllerView, self).get_context_data()
        try:
            current_data = requests.get(
                settings.SMART_HOME_API_URL,
                headers={'Authorization': f'Bearer {settings.SMART_HOME_ACCESS_TOKEN}'}
            ).json()
            context['data'] = current_data
        except:
            context['data'] = {}
        return context

    def get_initial(self):
        initial_data = super(ControllerView, self).get_initial()
        initial_data['bedroom_target_temperature'] = 21
        initial_data['hot_water_target_temperature'] = 80
        return initial_data

    def form_valid(self, form):
        try:
            bedroom_temp = Setting.objects.get(controller_name='bedroom_target_temperature')
        except Setting.DoesNotExist:
            Setting.objects.create(
                controller_name='bedroom_target_temperature',
                label='Bedroom target temperature',
                value=form.cleaned_data['bedroom_target_temperature']
            )
        else:
            bedroom_temp.value = form.cleaned_data['bedroom_target_temperature']
            bedroom_temp.save()

        try:
            hot_water_temp = Setting.objects.get(controller_name='hot_water_target_temperature')
        except Setting.DoesNotExist:
            Setting.objects.create(
                controller_name='hot_water_target_temperature',
                label='Hot water target temperature value',
                value=form.cleaned_data['hot_water_target_temperature']
            )
        else:
            hot_water_temp.value = form.cleaned_data['hot_water_target_temperature']
            hot_water_temp.save()

        controller_data = requests.get(
            URL,
            headers={'Authorization': f'Bearer {settings.SMART_HOME_ACCESS_TOKEN}'}
        ).json().get('data')

        if controller_data:
            bedroom_light = list(
                filter(lambda x: 'bedroom_light' in x.values(), controller_data)
            )[0]['value']
            bathroom_light = list(
                filter(lambda x: 'bathroom_light' in x.values(), controller_data)
            )[0]['value']
            smoke_detector = list(
                filter(lambda x: 'smoke_detector' in x.values(), controller_data)
            )[0]['value']
            json_data = {'controllers': []}
            if not smoke_detector:
                if form.cleaned_data['bedroom_light'] != bedroom_light:
                    json_data['controllers'].append(
                        {'name': 'bedroom_light', 'value': form.cleaned_data['bedroom_light']}
                    )
                if form.cleaned_data['bathroom_light'] != bathroom_light:
                    json_data['controllers'].append(
                        {'name': 'bathroom_light', 'value': form.cleaned_data['bathroom_light']}
                    )
                requests.post(
                    URL,
                    headers={'Authorization': f'Bearer {settings.SMART_HOME_ACCESS_TOKEN}'},
                    json=json_data
                )
        return super(ControllerView, self).get(form)
