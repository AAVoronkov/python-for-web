import base64
import json

from jsonschema import validate
from jsonschema.exceptions import ValidationError
from django import forms
from django.http import HttpResponse, JsonResponse
from django.views import View
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.forms.models import model_to_dict

from .models import Item, Review


class GoodsForm(forms.Form):
    title = forms.CharField(max_length=64)
    description = forms.CharField(max_length=1024)
    price = forms.IntegerField(min_value=1, max_value=1000000)


class ReviewForm(forms.Form):
    text = forms.CharField(max_length=1024)
    grade = forms.IntegerField(min_value=1, max_value=10)


GOODS_SCHEMA = {
    '$schema': 'http://json-schema.org/schema#',
    'type': 'object',
    'properties': {
        'title': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 64,
        },
        'description': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 1024,
        },
        'price': {
            'type': 'integer',
            'minimum': 1,
            'maximum': 1000000,
        },
    },
    'required': ['title', 'description', 'price'],
}


REVIEW_SCHEMA = {
    '$schema': 'http://json-schema.org/schema#',
    'type': 'object',
    'properties': {
        'text': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 1024,
        },
        'grade': {
            'type': 'integer',
            'minimum': 1,
            'maximum': 10,
        },
    },
    'required': ['text', 'grade'],
}


@method_decorator(csrf_exempt, name='dispatch')
class AddItemView(View):
    """View для создания товара."""

    def post(self, request):
        try:
            encoded = request.headers['Authorization'].split()[1]
            decoded = base64.b64decode(encoded).decode('utf-8')
            username = decoded.split(':')[0]
            password = decoded.split(':')[1]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                if user.is_staff:
                    document = json.loads(request.body)
                    validate(document, GOODS_SCHEMA)
                    item = Item(title=document['title'], description=document['description'], price=document['price'])
                    item.save()
                    item_id = Item.objects.filter(title=document['title']).values('id')[0]['id']
                    return JsonResponse({'id': item_id}, status=201)
                else:
                    return JsonResponse(data={}, status=403)
            else:
                return JsonResponse(data={}, status=401)
        except AttributeError:
            return JsonResponse(data={}, status=401)
        except json.JSONDecodeError:
            return JsonResponse({'errors': 'Invalid JSON'}, status=400)
        except ValidationError as exc:
            return JsonResponse({'errors': exc.message}, status=400)


@method_decorator(csrf_exempt, name='dispatch')
class PostReviewView(View):
    """View для создания отзыва о товаре."""

    def post(self, request, item_id):
        try:
            item = Item.objects.get(id=item_id)
            document = json.loads(request.body)
            validate(document, REVIEW_SCHEMA)
            document['item_id'] = item_id
            review = Review(text=document['text'], grade=document['grade'], item_id=document['item_id'])
            review.save()
            review_id = Review.objects.filter(item_id=document['item_id']).values('id').last()['id']
            return JsonResponse({'id': review_id}, status=201)
        except Item.DoesNotExist:
            return JsonResponse(data={}, status=404)
        except json.JSONDecodeError:
            return JsonResponse({'errors': 'Invalid JSON'}, status=400)
        except ValidationError as exc:
            return JsonResponse({'errors': exc.message}, status=400)


class GetItemView(View):
    """View для получения информации о товаре.

    Помимо основной информации выдает последние отзывы о товаре, не более 5
    штук.
    """

    def get(self, request, item_id):
        try:
            item = Item.objects.prefetch_related('review_set').get(id=item_id)
        except Item.DoesNotExist:
            return JsonResponse(data={}, status=404)
        items = model_to_dict(item)
        reviews = []
        for i in item.review_set.all():
            reviews.append(model_to_dict(i))
        reviews = sorted(reviews, key=lambda review: review['id'], reverse=True)[:5]
        for review in reviews:
            review.pop('item', None)
        items['reviews'] = reviews
        return JsonResponse(items, status=200)
