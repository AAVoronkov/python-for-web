import requests
from requests.auth import HTTPBasicAuth
import json


#url_1 = "https://datasend.webpython.graders.eldf.ru/submissions/1/"
#headers_1 = {'Authorization': 'Basic YWxsYWRpbjpvcGVuc2VzYW1l'}

#resp = requests.post(url_1, headers=headers_1)
#print(resp.text)

url_2 = "https://datasend.webpython.graders.eldf.ru/submissions/super/duper/secret/"
headers_2 = {'Authorization': 'galchonok ktotama'}

resp = requests.put(url_2, auth=HTTPBasicAuth('galchonok', 'ktotama'))
decoded = json.loads(resp.text)

with open('week05_01.txt', 'w') as f:
   f.write(decoded['answer'])

