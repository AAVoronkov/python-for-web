from bs4 import BeautifulSoup
from decimal import *
import requests


def convert(amount, cur_from, cur_to, date, requests):
    resp = requests.get("http://www.cbr.ru/scripts/XML_daily.asp?date_req=" + date)
    soup = BeautifulSoup(resp.content, "xml")
    if cur_from == 'RUR':
        nominal = Decimal(soup.find('CharCode', text=cur_to).find_next_sibling('Nominal').string)
        value = Decimal('.'.join(soup.find('CharCode', text=cur_to).find_next_sibling('Value').string.split(',')))
        result = amount * nominal / value
    elif cur_to == 'RUR':
        nominal = Decimal(soup.find('CharCode', text=cur_from).find_next_sibling('Nominal').string)
        value = Decimal('.'.join(soup.find('CharCode', text=cur_from).find_next_sibling('Value').string.split(',')))
        result = amount / nominal * value
    else:
        nominal_from = Decimal(soup.find('CharCode', text=cur_from).find_next_sibling('Nominal').string)
        value_from = Decimal('.'.join(soup.find('CharCode', text=cur_from).find_next_sibling('Value').string.split(',')))
        nominal_to = Decimal(soup.find('CharCode', text=cur_to).find_next_sibling('Nominal').string)
        value_to = Decimal('.'.join(soup.find('CharCode', text=cur_to).find_next_sibling('Value').string.split(',')))
        result = amount / nominal_from * value_from * (nominal_to / value_to)

    return result.quantize(Decimal('.0001'), rounding=ROUND_UP)