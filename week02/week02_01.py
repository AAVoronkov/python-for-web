from bs4 import BeautifulSoup
import re
import os


def build_tree(start, end, path):
    link_re = re.compile(r"(?<=/wiki/)[\w()]+")
    files = dict.fromkeys(os.listdir(path), False)
    initial_links = [start]
    while initial_links:
        total_links = []
        for name in initial_links:
            with open("{}{}".format(path, name), encoding='utf-8') as data:
                links = re.findall(link_re, data.read())
            for link in links:
                if files.get(link) is False:
                    files[link] = name
                    if link == end:
                        return files
                    total_links.append(link)
        initial_links = total_links


def build_bridge(start, end, path):
    files = build_tree(start, end, path)
    initial_link, bridge = end, [end]
    while initial_link != start:
        initial_link = files[initial_link]
        bridge.append(initial_link)
    return bridge


def parse(start, end, path):
    bridge = build_bridge(start, end, path)
    out = {}

    for file in bridge:
        with open("{}{}".format(path, file), encoding='utf-8') as data:
            soup = BeautifulSoup(data, "lxml")
            body = soup.find(id="bodyContent")
            images = len(body.find_all('img', width=lambda x: int(x or 0) > 199))
            headers = sum(1 for tag in body.find_all(['h1', 'h2', 'h3', 'h4', 'h5', 'h6']) if tag.get_text()[0] in "ETC")
            lists = sum(1 for tag in body.find_all(['ol', 'ul']) if not tag.find_parent(['ol', 'ul']))
            tag = body.find_next("a")
            links_length = -1
            while (tag):
                length = 1
                for tag in tag.find_next_siblings():
                    if tag.name != 'a':
                        break
                    length += 1
                if length > links_length:
                    links_length = length
                tag = tag.find_next("a")

            out[file] = [images, headers, links_length, lists]

    return out
